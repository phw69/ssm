package com.phw.entity;



import java.io.Serializable;


public class UserInfo implements Serializable{

    private Integer id;
    private String nick_name;
    private String sex;
    private Integer age;
    private Integer state;
    private Integer uid;

    @Override
    public String toString() {
        return "UserInfo{" +
                "id=" + id +
                ", nick_name='" + nick_name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", state=" + state +
                ", uid=" + uid +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}
