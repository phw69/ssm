package com.phw.dao;

import com.phw.entity.UserInfo;

import java.util.List;

public interface UserInfoDao {
    List<UserInfo> findAll();
}
