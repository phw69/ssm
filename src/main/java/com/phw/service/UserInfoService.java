package com.phw.service;

import com.phw.entity.UserInfo;

import java.util.List;

public interface UserInfoService {

    public List<UserInfo> findAll();

}
