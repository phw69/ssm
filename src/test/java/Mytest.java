import com.phw.dao.UserInfoDao;
import com.phw.entity.UserInfo;
import com.phw.service.UserInfoService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Mytest {
    @Test
    public void test(){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        SqlSessionFactory sqlSessionFactory = (SqlSessionFactory) applicationContext.getBean("SqlSessionFactory");
        SqlSession sqlSession = sqlSessionFactory.openSession();
        UserInfoDao mapper = sqlSession.getMapper(UserInfoDao.class);
        List<UserInfo> all = mapper.findAll();
        System.out.println(all);
    }
}
