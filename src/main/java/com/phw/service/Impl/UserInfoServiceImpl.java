package com.phw.service.Impl;


import com.phw.dao.UserInfoDao;
import com.phw.entity.UserInfo;
import com.phw.service.UserInfoService;

import java.util.List;

public class UserInfoServiceImpl implements UserInfoService {

    private UserInfoDao userInfoDao;

    public UserInfoDao getUserInfoDao() {
        return userInfoDao;
    }

    public void setUserInfoDao(UserInfoDao userInfoDao) {
        this.userInfoDao = userInfoDao;
    }

    @Override
    public List<UserInfo> findAll() {
        List<UserInfo> userInfos = userInfoDao.findAll();
        return userInfos;
    }
}
