package com.phw.controller;

import com.alibaba.fastjson.JSON;
import com.phw.entity.UserInfo;
import com.phw.service.UserInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("userinfo")
public class UserInfoController {

    private UserInfoService userInfoService;

    public void setUserInfoService(UserInfoService userInfoService) {

        this.userInfoService = userInfoService;
    }

    @GetMapping(value = "/findAll",produces = {"application/json;charset=utf-8"})
    public String findAll(){
        List<UserInfo> all = userInfoService.findAll();
        String s = JSON.toJSONString(all);
        return s;
    }
}
